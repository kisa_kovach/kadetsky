<?php
/**
 * Date: 02.09.2016
 * Time: 12:38
 */

require (__DIR__.'/inc/KadetskyThemeSetup.php');

$themeSetup = new KadetskyThemeSetup();
add_action('init',array(&$themeSetup,'__construct'));

function get_theme_slider_stick(){

    return '<h4>ЖИТЛОВИЙ КОМПЛЕКС</h4>
             <h2>"КАДЕТСЬКИЙ"</h2>
             <p>Комфортне житло в центрі Полтави</p>';
}