<?php
/**
 *
 * Date: 02.09.2016
 * Time: 12:40
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>">
    <head>
        <meta charset="utf-8" >
        <title><?php wp_title();?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
    </head>
    <body class="<?php body_class();?>">
        <div class="main">
            <header>
               <?php if(true){ ?>
                <div class="theme-slider">
                    <div class="home-slider-stick">
                    </div>
                    <?php
                        //if(function_exists('wd_slider'))
                             //wd_slider(1);
                    ?>
                    <div class="slider-stick">
                        <?php echo get_theme_slider_stick() ?>
                    </div>
                    <iframe src="https://www.youtube.com/embed/xYI1Lg_oWtw?autoplay=1&controls=0&autohide=1&showinfo=0">
                    </iframe>
                </div>
               <?php } ?>
                <div class="header-bottom">
                    <?php wp_nav_menu();?>
                    <div class="top-phone"><a class="top-phone-link" href="tel:+380675395039">+380 (67) 539 50 39</a></div>
                </div>
            </header>
            <div class="main-content">
