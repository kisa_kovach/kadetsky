<?php
/**
 * Theme setup
 * Date: 02.09.2016
 * Time: 15:18
 */

class KadetskyThemeSetup {

    public function __construct(){
       $this->themeSetup();
       add_action('init',array(&$this,'addSupportFlats'));
       add_action('wp_enqueue_scripts',array(&$this,'addScriptsAndStyles'));
       add_action('after_setup_theme',array(&$this,'themeSetup'));
    }

   public function themeSetup(){

   /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
    add_theme_support( 'title-tag' );

    load_theme_textdomain(get_template_directory() .'lang/kadetsky.mo');
    /*
    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
    add_theme_support( 'html5', array(
           'search-form',
           'comment-form',
           'comment-list',
           'gallery',
           'caption',
       ) );

       /*
        * Enable support for Post Formats.
        *
        * See: https://codex.wordpress.org/Post_Formats
        */
    add_theme_support( 'post-formats', array(
           'aside',
           'image',
           'video',
           'quote',
           'link',
           'gallery',
           'status',
           'audio',
           'chat',
       ) );

    $this->addMenuContainers();

   }

   public function addScriptsAndStyles() {
       wp_enqueue_style('normalize',get_bloginfo('template_url').'/css/normalize.css');
       wp_enqueue_style( 'kadetsky-style', get_stylesheet_uri() );
       wp_enqueue_style( 'kadetsky-mobile',get_bloginfo('template_url').'/css/responsive.css');
       wp_enqueue_style('font-awesome',get_bloginfo('template_url').'/css/font-awesome.min.css');
       wp_enqueue_script('main',get_bloginfo('template_url').'js/main.js',null,false,true);

   }

   public function addMenuContainers(){
       register_nav_menus(
           array(
           'top_menu' =>__('top menu','kadetsky'),
           'bottom_menu' =>__('bottom menu','kadetsky'),)
       );
   }

   /* Register post types */

   public function addSupportFlats(){
       $args=array(
           'label'  => __('flat','kadetsky'),
           'labels' => array(
               'name'               => __('flats','kadetsky'), // основное название для типа записи
               'singular_name'      => __('flat','kadetsky'), // название для одной записи этого типа
               'add_new'            => __('add flat','kadetsky'), // для добавления новой записи
               'add_new_item'       => __('New flat','kadetsky'), // заголовка у вновь создаваемой записи в админ-панели.
               'edit_item'          => __('flat','kadetsky'), // для редактирования типа записи
               'new_item'           => '', // текст новой записи
               'view_item'          => __('view flat','kadetsky'), // для просмотра записи этого типа.
               'search_items'       => __('search flats','kadetsky'), // для поиска по этим типам записи
               'not_found'          => __('no flats found','kadetsky'), // если в результате поиска ничего не было найдено
               'not_found_in_trash' => __('no flats found in trash','kadetsky'), // если не было найдено в корзине
           ),
           'description'         => '',
           'public'              => true,
           'publicly_queryable'  => null,
           'exclude_from_search' => null,
           'show_ui'             => null,
           'show_in_menu'        => null,
           'menu_position'       => null,
           'menu_icon'           => null,
           //'capability_type'   => 'post',
           //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
           //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
           'hierarchical'        => false,
           'supports'            => array('title','editor'),
           'taxonomies'          => array('category'),
           'has_archive'         => false,
           'rewrite'             => true,
           'query_var'           => true,
           'show_in_nav_menus'   => false,

       );

       register_post_type('flat',$args);
   }
}