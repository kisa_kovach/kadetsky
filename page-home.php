<?php
/**
 *
 *  Base template for home-page
 *
 */

get_header();
?>
<div class="one-page-app">
    <div class="promo-box">
        <h2 class="text-center">Купуючи квартиру в ЖК ``КАДЕТСЬКИЙ`` Ви отримуєте:</h2>
        <div class="star-delimiter text-center"><i class="dft dft-star"></i></div>
    </div>
</div>
